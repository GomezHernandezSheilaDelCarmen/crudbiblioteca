<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];
  $error = false;
  if (empty($clave_ejemplar)) {
    $error = true;
?>
  <p>Error, no se ha indicado la clave del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar, isbn
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
      $error = true;
?>
  <p>No se ha encontrado algún ejemplar con clave<?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
      $isbn = $tupla['isbn'];
?>
<table>
  <caption>Información de ejemplar</caption>
  <tbody>
    <tr>
      <th>Clave del ejemplar</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>Conservacion del ejemplar</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Libro/s</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro
        where isbn = '".$isbn."';";

      $libros = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>Sin libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
          $titulo_libro = $tupla['titulo_libro'];
?>
          <li><?php echo $titulo_libro.; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="clave_ejemplar" value="<?php echo $clave_ejemplar; ?>" />
  <p>¿Está seguro/a de eliminar este ejemplar?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez  la relación que mantenga con sus libros.
  </p>
</form>

<form action="ejemplares.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>
